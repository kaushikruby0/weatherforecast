//
//  WeatherCollectionViewCell.swift
//  WeatherForecasting
//
//  Created by Ruby Kaushik on 02/07/16.
//  Copyright © 2016 Ruby Kaushik. All rights reserved.
//

import UIKit

class WeatherCollectionViewCell: UICollectionViewCell  , UICollectionViewDataSource , UICollectionViewDelegate , UICollectionViewDelegateFlowLayout{
    var dataArray : Array<WeatherModal> = []
    
    @IBOutlet weak var clctn : UICollectionView!


    override func awakeFromNib() {
        super.awakeFromNib()
       

        clctn.registerNib(UINib(nibName: "WeatherDetailCollectionCell", bundle: nil), forCellWithReuseIdentifier: CollectionDetailCellIdentifier)
                
    }
    
    func configureCollectioncell(modalArray : Array<WeatherModal>)
    {
        dataArray = modalArray
        clctn.delegate = self
        clctn.dataSource = self
        clctn.reloadData()
    }
    
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataArray.count

    }
    
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(CollectionDetailCellIdentifier, forIndexPath: indexPath) as? WeatherDetailCollectionCell
        
        let item = dataArray[indexPath.row]
        cell?.configureTableCell(item)
        
        return cell!
        
    }
    
     func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        
        let width = collectionView.frame.size.width
        
        return CGSizeMake(width , 75)
        
        
    }

    
    
  }
