//
//  WeatherTableViewCell.swift
//  WeatherForecasting
//
//  Created by Ruby Kaushik on 02/07/16.
//  Copyright © 2016 Ruby Kaushik. All rights reserved.
//

import UIKit

class WeatherTableViewCell: UITableViewCell {

    @IBOutlet weak var dayLabel: UILabel!
    @IBOutlet weak var tempLabel: UILabel!
    @IBOutlet weak var summaryLabel: UILabel!
    @IBOutlet weak var windLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
       selectionStyle = .None
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureTableCell(modal : WeatherModal?)
    {
        if modal != nil{
            
            self.tempLabel.text = "\(modal!.tempMax)ºC | \(modal!.tempMin)ºC"
            self.summaryLabel.text = modal!.summary
            self.windLabel.text = "\(modal!.speed) km/hE"
//            self.dayLabel.text = modal!.timeInterval.getStringDateFromTimeInterval()
            
        }
    }
    
}
