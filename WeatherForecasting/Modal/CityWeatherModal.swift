//
//  CityWeatherModal.swift
//  WeatherForecasting
//
//  Created by Ruby Kaushik on 03/07/16.
//  Copyright © 2016 Ruby Kaushik. All rights reserved.
//

import Foundation


struct CityWeatherModal {
    
    var city : String
    var weatherModalArray : Array<WeatherModal>
}


extension CityWeatherModal : JSONDecodable
{
    
    init?(JSON: [String : AnyObject]) {
        
        guard let city = JSON["city"]?["name"] as? String , let weatherModalArray = JSON["list"] as? Array<[String : AnyObject]> else
        {
            return nil
        }
        
        self.city = city
        self.weatherModalArray = []
        for dict in weatherModalArray
        {
            if let weatherModal = WeatherModal(JSON: dict)
            {
                self.weatherModalArray.append(weatherModal)
            }
            
        }
        
    }
}
