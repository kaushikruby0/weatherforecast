//
//  WeatherIcon.swift
//  WeatherForecasting
//
//  Created by Ruby Kaushik on 02/07/16.
//  Copyright © 2016 Ruby Kaushik. All rights reserved.
//

import Foundation
import UIKit


enum WeatherIcon : String
{
    case Clear = "clear sky"
    case Rainy  = "Rain"
    case Snow = "snow"
    case Sleet = "sleet"
    case PartlyCloudy = "partlyCloudy"
    case Cloudy = "cloudy"
    case Unexpected = "default"
    
    init(rawValue : String)
    {
        switch rawValue
        {
        case "clear":
            self = .Clear
        case "Rain":
            self = .Rainy
        case "snow":
            self = .Snow
        case "Sleet":
            self = .Sleet
        case "PartlyCloudy":
            self = .PartlyCloudy
        case "Cloudy":
            self = .Cloudy
        default :
            self = .Unexpected
        }
    }
}

extension WeatherIcon
{
    var image : UIImage
        {
        return UIImage(named: "default")!
    }
}