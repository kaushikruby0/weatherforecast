//
//  WeatherModal.swift
//  WeatherForecasting
//
//  Created by Ruby Kaushik on 02/07/16.
//  Copyright © 2016 Ruby Kaushik. All rights reserved.
//

import Foundation

struct WeatherModal {
    
    
    var tempMin : Double
    var tempMax : Double
    var humidity : Double
    var speed : Double
    var summary : String
   // var icon : UIImage
    var timeInterval : Double

}




extension WeatherModal : JSONDecodable
{
    init?(JSON : [String : AnyObject])
    {
        guard let tempMin = JSON["temp"]?["min"] as? Double , let timeInterval = JSON["dt"] as? Double ,let tempMax = JSON["temp"]?["max"] as? Double, speed = JSON["speed"] as? Double , let humidity = JSON["humidity"] as? Double , let summary = (JSON["weather"] as? Array<[String : AnyObject]>)?.first?["main"]  as? String else
        {
            return nil
        }
        
        
        self.tempMax = tempMax
        self.tempMin = tempMin
        self.humidity = humidity
        self.speed = speed
        self.summary = summary
       // self.icon = WeatherIcon.init(rawValue: summary).image
        self.timeInterval = timeInterval
        
    }

}
