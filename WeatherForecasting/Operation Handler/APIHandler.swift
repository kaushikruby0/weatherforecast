//
//  APIHandler.swift
//  WeatherForecasting
//
//  Created by Ruby Kaushik on 02/07/16.
//  Copyright © 2016 Ruby Kaushik. All rights reserved.
//

import Foundation

public let ErrorDomain = "com.MyOrg.WeatherForecasting.NetworkError"
public let MissingResponseError = 10
public let UnexpectedError = 20

typealias JSON = [String : AnyObject]
typealias JSONTaskCompletion =   (JSON? , NSHTTPURLResponse? , NSError? )-> Void
typealias JSONTask = NSURLSessionDataTask


struct Coordinate {
    
    let latitude : Double
    let longitude : Double
}



enum APIResult<T>
{
    case Success(T)
    case Failure(ErrorType)
}

protocol JSONDecodable
{
    init?(JSON : [String : AnyObject])
}

protocol EndPoint
{
    var baseURL : NSURL {get}
    var path : String { get }
    var request : NSURLRequest {get}
}


enum MakeAPI : EndPoint
{
    case CoordinateType(key : String , coordinate : Coordinate)
    case CityType(key : String , city : String)
    
    var baseURL : NSURL
        {
            return NSURL(string: BASE_URL)!
        }
    
    var path : String
        {
            switch self
            {
            case .CoordinateType(let key, let coordinate) :
                let path = "?lat=\(coordinate.latitude)&lon=\(coordinate.longitude)&APPID=\(key)&cnt=14&units=metric"
                return path
                
            case .CityType(let key, let city) :
                
                let trimmedCity = city.stringByReplacingOccurrencesOfString(" ", withString: "")
                
                let path = "?q=\(trimmedCity)&APPID=\(key)&cnt=14&units=metric"
                return path
            }
    }

    var request : NSURLRequest
        {
            let url = NSURL(string: path, relativeToURL: baseURL)
            let request = NSURLRequest(URL: url!)
            return request
    }
    
}



