//
//  OperationHandler.swift
//  WeatherForecast
//
//  Created by Ruby Kaushik on 29/06/16.
//  Copyright © 2016 Ruby Kaushik. All rights reserved.
//

import UIKit
import SystemConfiguration

class OperationHandler: NSObject , NSURLSessionDelegate {
    
    static let sharedInstance = OperationHandler()
    
    private override init() {
    }
    
    private func isConnectionAvailable() -> Bool {
        
        var zeroAddress = sockaddr_in(sin_len: 0, sin_family: 0, sin_port: 0, sin_addr: in_addr(s_addr: 0), sin_zero: (0, 0, 0, 0, 0, 0, 0, 0))
        zeroAddress.sin_len = UInt8(sizeofValue(zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(&zeroAddress) {
            SCNetworkReachabilityCreateWithAddress(kCFAllocatorDefault, UnsafePointer($0))
        }
        
        var flags: SCNetworkReachabilityFlags = SCNetworkReachabilityFlags(rawValue: 0)
        if SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) == false {
            return false
        }
        
        let isReachable = flags == .Reachable
        let needsConnection = flags == .ConnectionRequired
        
        return isReachable && !needsConnection
        
    }
    
    func getSessionInstance() -> NSURLSession
    {
        let configuration = NSURLSessionConfiguration.defaultSessionConfiguration()
        let session = NSURLSession(configuration: configuration)
        
        return session
    }
    
    
        func getWeatherCity(city : String ,  successBlock : ((result : CityWeatherModal?)-> Void))
        {
            if !isConnectionAvailable()
            {
                UtilityClass.sharedInstance.showAlertViewWithMessage(NO_INTERNET_CONNECTION_ALERT_MSG)
                return
            }
            let request = MakeAPI.CityType(key: API_KEY, city: city).request
            
            
            let urlSession = getSessionInstance()
            
            let task = urlSession.dataTaskWithRequest(request) { (data, response, error) -> Void in
                
                if data != nil
                {
                     do
                     {
                        
                        if let json = try NSJSONSerialization.JSONObjectWithData(data!, options: []) as? [String : AnyObject]
                        {
                            if (json[STATUS_CODE] as? String) == "200"
                            {
                                print("data ==== ",json[STATUS_CODE])
                                
                                if let cityWeatherModal = CityWeatherModal(JSON: json)
                                {
                                    successBlock(result: cityWeatherModal)
                                    
                                }
                                else
                                {
                                    UtilityClass.sharedInstance.showAlertViewWithMessage("Something is wrong")
                                }
                                
                            }

                        }
                            
                    }
                    catch let error as NSError
                    {
                        UtilityClass.sharedInstance.showAlertViewWithMessage(error.localizedDescription)
                    }

                }
            
            }
            task.resume()
          
        }

}
