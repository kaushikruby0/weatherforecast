//
//  Constant.swift
//  WeatherForecast
//
//  Created by Ruby Kaushik on 30/06/16.
//  Copyright © 2016 Ruby Kaushik. All rights reserved.
//

import Foundation
import UIKit
//SERVER SETTINGS//

let BASE_URL = "http://api.openweathermap.org/data/2.5/forecast/daily"

let API_KEY = "f9a6ad272310d46c6cc735cdecd3e9a9"

let APP_NAME = "Weather Forecast"

//Objects

let APP_DELEGATE : AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate


//Identifiers

let collectionCellIdentifier = "CollectionCellIdentifier"
let CollectionDetailCellIdentifier = "CollectionDetailCellIdentifier"
let LocationnTableCellIdentifier = "TableCellIdentifier"


//Notifications

let LOCATION_ADDED_NOTIFICATION = "locationAdded"


//Error Messages

let NO_INTERNET_CONNECTION_ALERT_MSG = "You are currently offline. Please check your internet connection and try again."

let LOCATION_ACCESS_DENIED = "Enable the location settings of the application or add location "

let LOCATION_DISABLE = "Enable the location of your phone or add location to know the weather "

let ERROR_IN_UPDATING_LOCATION = "Error in updating location . Check internet connection"


//Status

let STATUS_CODE = "cod"




