//
//  UtilityClass.swift
//  WeatherForecast
//
//  Created by Ruby Kaushik on 30/06/16.
//  Copyright © 2016 Ruby Kaushik. All rights reserved.
//

import UIKit

class UtilityClass: NSObject {
    
    static let sharedInstance = UtilityClass()
    private let rootVC = APP_DELEGATE.window?.rootViewController
    
    private override init() {
    }
   
    
    func showAlertViewWithMessage(message : String )
    {
        let alerController = UIAlertController(title: APP_NAME, message: message, preferredStyle: .Alert)
        
        alerController.addAction(UIAlertAction(title: "OK", style: .Default, handler: { (action) -> Void in
            
        }))

       rootVC?.presentViewController(alerController, animated: true, completion: nil)
    }
    
}


extension String
{
    func isAlphabet() -> Bool
    {
        let decimalCharacters = NSCharacterSet.letterCharacterSet().invertedSet
        if self.rangeOfCharacterFromSet(decimalCharacters) == nil
        {
            return true
        }
        return false
    }
}


extension Double{
    
    func getStringDateFromTimeInterval() -> String
    {
        let date = NSDate(timeIntervalSince1970: self)
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.timeZone = NSTimeZone.systemTimeZone()
        dateFormatter.dateFormat = "dd MMM, yyyy"
        
        return dateFormatter.stringFromDate(date)
    }
}


