//
//  AddLocationVC.swift
//  WeatherForecasting
//
//  Created by Ruby Kaushik on 03/07/16.
//  Copyright © 2016 Ruby Kaushik. All rights reserved.
//

import UIKit

class AddLocationVC: UIViewController , UITextFieldDelegate{

    
    @IBOutlet private weak var locationTxtFld : UITextField!
    
    
    //MARK: - View Life Cycle Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Add Location"
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK: - Text Field Delegate

    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        
        if string.isEmpty || string.isAlphabet()
        {
            return true
        }
        return false
        
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    
    //MARK: - Post Notification
    
    func postNotification()
    {
        NSNotificationCenter.defaultCenter().postNotificationName(LOCATION_ADDED_NOTIFICATION, object: locationTxtFld.text!)
    }
    
    
    //MARK: - Button Action Methods
    
    /*
    Post the notification LOCATION_ADDED_NOTIFICATION and will pop the view controllwe
    */
    
    @IBAction func addLocationBtnTapped(sender: AnyObject) {
        
        if locationTxtFld.text?.characters.count > 0
        {
            postNotification()
            self.navigationController?.popViewControllerAnimated(true)
        }
        else
        {
            print("Location Cannot be empty")
        }
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
