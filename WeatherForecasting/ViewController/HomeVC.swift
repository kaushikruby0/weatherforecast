//
//  HomeVC.swift
//  WeatherForecast
//
//  Created by Ruby Kaushik on 28/06/16.
//  Copyright © 2016 Ruby Kaushik. All rights reserved.
//

import UIKit
import CoreLocation

class HomeVC: UIViewController , UICollectionViewDataSource , UICollectionViewDelegate  , UICollectionViewDelegateFlowLayout , CLLocationManagerDelegate {

    @IBOutlet weak var clctnView : UICollectionView!

    private let locationManager = CLLocationManager()

    private var cityWeatherModalArray : Array<CityWeatherModal> = []
    
    private var locality = ""
    var token : dispatch_once_t = 0


    //MARK: - View Life Cycle Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
            registerNibForCollectionView()
            getLocation()
            addObserver()
        
           }
    
    override func viewWillAppear(animated: Bool) {
        
        super.viewWillAppear(animated)
        
        configureNavigationBar()
        clctnView.reloadData()
    }
    
    
    deinit
    {
        removeObserver()
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    
    
    
    //MARK: - Configuration Methods
    
    func configureNavigationBar()
    {
        let addBtn = UIBarButtonItem(barButtonSystemItem: .Add, target: self, action: "addBarButtonItemTapped")
        self.navigationItem.leftBarButtonItem = addBtn
    }
    
    
    func setNavigationTitle(title : String)
    {
        self.navigationItem.title = title

    }
    
    
    func registerNibForCollectionView()
    {
        clctnView.delegate = self
        clctnView.dataSource = self
        
        clctnView.registerNib(UINib(nibName:"WeatherCollectionViewCell", bundle: nil), forCellWithReuseIdentifier:collectionCellIdentifier )
    }
    
    //MARK: - Location Methods
    
    func getLocation()
    {
        //Location services are enabled in settings of phone
        if CLLocationManager.locationServicesEnabled() {
            
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyThreeKilometers
            
            if locationManager.respondsToSelector("requestAlwaysAuthorization")
            {
                locationManager.requestAlwaysAuthorization()
                
            }
            locationManager.startUpdatingLocation()
        }
            
        //Location services are disabled in settings of phone

        else
        {
            UtilityClass.sharedInstance.showAlertViewWithMessage(LOCATION_DISABLE)
        }
    }
    
    
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let location = locations.first
        CLGeocoder().reverseGeocodeLocation(location!) { (placemarks, error) -> Void in
            
            if (error != nil)
            {
                dispatch_once(&self.token , { () -> Void in
                    UtilityClass.sharedInstance.showAlertViewWithMessage(ERROR_IN_UPDATING_LOCATION)
                })
                return
            }
            if placemarks?.count > 0
            {
                let currentPlaceMark = placemarks?.first
                if let locality = currentPlaceMark?.locality
                {
                    self.locality = locality
                }
                self.getWeathedataForCity(self.locality)
                self.locationManager.stopUpdatingLocation()
            }
           
        }
        
    }
    
    func locationManager(manager: CLLocationManager, didFailWithError error: NSError) {
        
        if CLLocationManager.authorizationStatus() == CLAuthorizationStatus.Denied || CLLocationManager.authorizationStatus() == CLAuthorizationStatus.Restricted
        {
            UtilityClass.sharedInstance.showAlertViewWithMessage(LOCATION_ACCESS_DENIED)
        }
    }

    
    //MARK: - UICollectionView Datasource & Delegates
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return cityWeatherModalArray.count
    
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(collectionCellIdentifier, forIndexPath: indexPath) as? WeatherCollectionViewCell
        
        let cityModal = cityWeatherModalArray[indexPath.row]
        
        let weatherModalArray = cityModal.weatherModalArray
        
        if self.navigationItem.title == nil
        {
            setNavigationTitle(cityModal.city)
        }
        
        if weatherModalArray.count > 0
        {
             cell?.configureCollectioncell(weatherModalArray)
        }
        
        return cell!
        
    }
    
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        
        
        let height = collectionView.frame.size.height - (collectionView.contentInset.top + collectionView.contentInset.bottom)
        
        let width = collectionView.frame.size.width
        
        return CGSizeMake(width , height)
    }
    
    //MARK: - Scroll View

    func scrollViewDidScroll(scrollView: UIScrollView) {
        
        let index = Int(floor(scrollView.contentOffset.x / clctnView.frame.size.width))
        if index >= 0 && cityWeatherModalArray.count > 0
        {
            let cityModal = cityWeatherModalArray[index]
            setNavigationTitle(cityModal.city)
        }
    }

    
   //MARK: - Web service call
    
    func getWeathedataForCity(city : String)
    {
        OperationHandler.sharedInstance.getWeatherCity(city){(result) -> Void in
            
            if result != nil{
                
                if !self.cityWeatherModalArray.contains({$0.city == result?.city})
                {
                    self.cityWeatherModalArray.append(result!)
                }
                
            }
            
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                
                self.clctnView.reloadData()
                if self.cityWeatherModalArray.count > 0
                {
                    self.clctnView.reloadItemsAtIndexPaths([NSIndexPath(forItem: self.cityWeatherModalArray.count - 1, inSection: 0)])
                }
            })
            
        }
    }
    
    
    
    //MARK: - Notifocation Oserver
    
    
    func addObserver()
    {
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "locationAdded:", name: LOCATION_ADDED_NOTIFICATION, object: nil)
    }
    
    func removeObserver() {
        
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    //Target of the LOCATION_ADDED_NOTIFICATION
    
    func locationAdded(notification : NSNotification) {
        
        let city = notification.object as! String
        
        getWeathedataForCity(city)
        
    }

    
    //MARK: - Button Actions
    
    func addBarButtonItemTapped()
    {
        let locationsVC = LocationsVC(nibName : "LocationsVC" , bundle:  nil)
        
        for object in cityWeatherModalArray
        {
            locationsVC.cityNamesArray.append(object.city)
        }
        
        self.navigationController?.pushViewController(locationsVC, animated: true)
    }
}
