//
//  LocationsVC.swift
//  WeatherForecasting
//
//  Created by Ruby Kaushik on 03/07/16.
//  Copyright © 2016 Ruby Kaushik. All rights reserved.
//

import UIKit

class LocationsVC: UIViewController  , UITableViewDataSource , UITableViewDelegate{
    
    
    @IBOutlet weak var locationtblView : UITableView!
    
    var cityNamesArray : Array<String> = []
    
    //MARK: - View Life Cycle Methods

    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Locations"
        addObserver()
    }
    
    
    deinit
    {
        removeObserver()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK: - Notification Observer
    
    func addObserver()
    {
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "locationAdded:", name: LOCATION_ADDED_NOTIFICATION, object: nil)
    }
    
    func removeObserver() {
        
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    func locationAdded(notification : NSNotification) {
        
        let city = notification.object as! String
        
        cityNamesArray.append(city)
        
        locationtblView.reloadData()
    }
    

    //MARK: - TableView Delegate Methods
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return cityNamesArray.count
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
     
        var cell = tableView.dequeueReusableCellWithIdentifier(LocationnTableCellIdentifier)
        
        if cell == nil{
            cell = UITableViewCell(style: .Default, reuseIdentifier: LocationnTableCellIdentifier)
        }

        cell?.textLabel?.text = cityNamesArray[indexPath.row]
    
        return cell!
    }
    
    
    
    //MARK: - Button Action Methods
    
    @IBAction func addBtnTapped(sender: AnyObject) {
        
        let addLocationVC = AddLocationVC(nibName : "AddLocationVC" , bundle:  nil)
        self.navigationController?.pushViewController(addLocationVC, animated: true)
    }
   
}
